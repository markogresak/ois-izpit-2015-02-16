var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	// ...
	// ?ds=34890&ime=hkdg&priimek=jk&ulica=kgjk&hisnaStevilka=12&postnaStevilka=1000&kraj=Ljubljana&drzava=Mad%C5%BEarska&poklic=PAPIRNI%C5%A0KI+DELAVEC&telefonskaStevilka=2390617896
	var ds = req.param('ds');
	var ime = req.param('ime');
	var priimek = req.param('priimek');
	var ulica = req.param('ulica');
	var hisnaStevilka = req.param('hisnaStevilka');
	var postnaStevilka = req.param('postnaStevilka');
	var kraj = req.param('kraj');
	var drzava = req.param('drzava');
	var poklic = req.param('poklic');
	var telefonskaStevilka = req.param('telefonskaStevilka');
	if(ds && ime && priimek && ulica && hisnaStevilka && kraj && drzava && poklic && telefonskaStevilka) {
		if(najdiIndexUpoarbnika(ds) !== -1) {
			return res.send("Oseba z davčno številko "+ds+" že obstaja!<br/><a href='javascript:window.history.back()'>Nazaj</a>");
		}
		var oseba = {davcnaStevilka: ds, ime: ime, priimek: priimek, naslov: ulica, hisnaStevilka: hisnaStevilka, postnaStevilka: postnaStevilka, kraj: kraj, drzava: drzava, poklic: poklic, telefonskaStevilka: telefonskaStevilka};
		console.log('dodaj', oseba);
		uporabnikiSpomin.push(oseba);

		res.redirect('/');
	}
	else {
		res.send("Napaka pri dodajanju osebe!");
	}
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	// ...
	var davcna = req.param('id');
	if(!davcna) {
		return res.send("Napačna zahteva!");
	}
	var id = najdiIndexUpoarbnika(davcna);
	if(id !== -1) {
		uporabnikiSpomin.splice(id, 1);
		res.redirect('/');
	}
	else {
		res.send("Oseba z davčno številko "+davcna+" ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
	}
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');

var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'},
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];

function najdiIndexUpoarbnika(davcnaStevilka) {
	if(davcnaStevilka) {
		davcnaStevilka = davcnaStevilka.toString();
		for(var i = 0; i < uporabnikiSpomin.length; i++) {
			if(uporabnikiSpomin[i].davcnaStevilka === davcnaStevilka) {
				return i;
			}
		}
	}

	return -1;
}